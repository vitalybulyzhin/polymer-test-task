const { expect } = require("chai");
const { ethers } = require("hardhat");
const testDeploy = require('../utils/testDeploy')
const blockTime = require('../utils/testBlockTime')
const TestPriceCalculator = require('../utils/testPrice')
const moment = require('moment');

describe("DutchAuction tests", function () {
    const price = 1000;
    const startTime = moment().unix() + 10;
    const endTime = moment().unix() + 20;
    const testPrice = new TestPriceCalculator(price, startTime, endTime)
    let contract;

    before(async () => {
        await ethers.provider.send('evm_setNextBlockTimestamp', [startTime - 5]);
        await network.provider.send("evm_mine")
        contract = await testDeploy(price, startTime, endTime)
    });

    it("Should get correct price at startTime", async () => {
        let timeNow;
        let price;

        while(startTime != timeNow) {
            await blockTime(startTime)
            
            const{ _price, _timeNow }= await contract.getPrice()
            price = _price
            timeNow = JSON.parse(_timeNow)
        }

        const value = JSON.parse(price)
        expect(value).equal(testPrice.at())
    })

    it("Should get correct price at endTime", async () => {
        let timeNow;
        let price;

        while(endTime != timeNow) {
            await blockTime(endTime) 
            
            const{ _price, _timeNow }= await contract.getPrice()
            price = _price
            timeNow = JSON.parse(_timeNow)
        }

        const value = JSON.parse(price)
        expect(value).equal(testPrice.at(endTime))
    })


    it("Should send correct formula to calculate price", async () => {
        await blockTime(startTime + 5)

        const { _price, formula, _a, _b, _timeNow } = await contract.getPrice()
        const priceValue = JSON.parse(_price)
        const aValue = JSON.parse(_a)
        const bValue = JSON.parse(_b)
        const timeNowValue = JSON.parse(_timeNow)

        const calculatedPrice = eval("(" + formula + ")")
        const priceFromFormula = calculatedPrice(aValue, bValue, timeNowValue)

        expect(priceFromFormula).equal(priceValue)
    })

    it("Should get initial balanace successfuly", async () => {
        const balance = await contract.getBalance()
        expect(balance).equal(0)
    })

    it("Should throw en error if balance requested not by the owner", async () => {
        try{
            [_, addr1] = await ethers.getSigners();
            await contract.connect(addr1).getBalance()
            expect(true).equal(false)
        } catch(e) {
            expect(e.message).not.equal("expected true to equal false")
        }
    })

    it("should not bit successfully if the value is too small", async () => {
        try {
            const send = await contract.bid({ value: 1 })
            await send.wait();
            expect(false).equal(true)
        } catch(e) {
            expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'Insufficient balance has been send'")
        }
    })

    it("Should not bid successfully if the auction has ended", async () => {
        try {
            await blockTime(endTime)
            const bidEtherumForVTT = await contract.bid({ value: price })
            await bidEtherumForVTT.wait();
            expect(true).equal(false)
        } catch(e) {
            expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'Sorry, auction has ended'")
        }
    })

    it("Should not bid successfully if the auction hasn't started", async () => {
        try {
            await blockTime(startTime - 10)
            const bidEtherumForVTT = await contract.bid({ value: price })
            await bidEtherumForVTT.wait();
            expect(true).equal(false)
        } catch(e) {
            expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'Auction hasn't started yet'")
        }
    })

    it("Should bid successfully", async () => {
        await blockTime(startTime + 5);
        [owner] = await ethers.getSigners();
        const { _, formula, _a, _b, _timeNow } = await contract.getPrice()
        const aValue = JSON.parse(_a)
        const bValue = JSON.parse(_b)
        const timeNowValue = JSON.parse(_timeNow)
        const calculatedPrice = eval("(" + formula + ")")
        const priceFromFormula = calculatedPrice(aValue, bValue, timeNowValue)

        const bidEtherumForVTT = await contract.bid({ value: priceFromFormula })
        await bidEtherumForVTT.wait();

        const contractEthereumBalance = await contract.connect(owner).getBalance()
        expect(priceFromFormula).equal(contractEthereumBalance)

        const response = await contract.balanceOf(owner.address)
        const receivedCustoERCToken = JSON.parse(response)
        expect(receivedCustoERCToken).equal(1)
    })

    it("Should bid unsuccessfully when the lot has been taken", async () => {
        try {
            const bidEtherumForVTT = await contract.bid({ value: price })
            await bidEtherumForVTT.wait();
            expect(true).equal(false)
        } catch(e) {
            expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'Sorry, no lots avalible'")
        }
    })

})
