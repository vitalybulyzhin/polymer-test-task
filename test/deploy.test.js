const { expect } = require("chai");
const testDeploy = require('../utils/testDeploy')
const moment = require('moment');

describe("DutchAuction deploy tests", function () {
  beforeEach(async function () {
    await network.provider.send("hardhat_reset")
  })

  it("Should deploy contract successfully", async () => {
    const deployed = await testDeploy(1, moment().unix() + 10, moment().unix() + 20);
    expect(deployed.address).not.be.undefined;
  });

  it ("Should not deploy contract is the the price is smaller than 1", async () => {
    try {
      testDeploy(0, moment().unix() + 10, moment().unix() + 20);
    } catch(e) {
      expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'Start price should be bigger than 0'")
    }
  })

  it ("Should not deploy contract is the the start time is invalid", async () => {
    try {
      testDeploy(1, moment().unix() - 10, moment().unix() + 20);
    } catch(e) {
      expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'Start date/time is not valid'")
    }
  })

  it ("Should not deploy contract is the the end time is invalid", async () => {
    try {
      testDeploy(1, moment().unix() + 10, moment().unix() - 10);
    } catch(e) {
      expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'End date/time is not valid'")
    }
  })

  it ("Should not deploy contract is the the end time is sooner than start time", async () => {
    try {
      testDeploy(1, moment().unix() + 10, moment().unix() + 9);
    } catch(e) {
      expect(e.message).equal("VM Exception while processing transaction: reverted with reason string 'End date/time should be bigger than start date/time'")
    }
  })
});
