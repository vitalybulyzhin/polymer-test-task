# Dutch Auction

## Stack
You mentioned in the task description that Solidity and Hardhat are okay for this task, and because this is the only stack I'm familiar with so far, so I decided to go with it. All the tests are written with pure JS (usually I use TS for my tests, but since it's test task, pure JS should fine)

## Prerequisites

To run tests, mack sure you have:
- Node v14.19.1 or higher installed 
- npm v6.14.16 or higher
- npx v6.14.16 or higher
- hardhat v2.9.3 (installed globally)

## Contract Implementation

### Assumptions

This contract implements WTIA dutch auction. In the task description there were not specific instructions about some details of implementation, so there are some details on this implementation:
- One contract - one auction. This basically means that when the auction is deployed and one auction is started - you cannot reuse this contract for another auction. I definitely won't design it this way for production, but I think this is good enough for the scope of test task
- One winner. There were no specific details about this, so I scoped it to only one winner of this auction
- You cannot participate in auction before it started
- You cannot participate in auction after it ends
- You can deploy contract only with valid start time, end time and price should be bigger than zero. Otherwise your contract still simply won't be deployed
- Contract balance is accessible only for the owner
- As a pot there is 1 custom ERC20 token
- There is no withdraw function, I'm aware it's necessary but I's not directly related to dutch auction, so I decided to skip it in the scope of the test task

### Actual implementation details
- getPrice - returns current price of the asset and everything to calculate price on the front-end. Since contract is time dependent, there need to be a way co synchronise the price to supply value. There is probably a way to do that through sockets (not sure if it's possible)no way they can cheat on this (but if they supply more money - it's actually fine)
- bid - checks if the auction is currently running (check time, if no one has takes assets yet and price they supply)
- getBalance - just getting balance if the contract (available only for owner)
- validateInput - checks for deployment of the contract

## Tests

There are 2 main test cases - tests for deploy and contract tests. Deploy tests mainly validateInput function and making sure contract cannot be deployed with invalid data. Contract tests cover all the methods of the contract

## Run tests

First of all, make sure you have everything installed that is listed in Prerequisites and install all the dependencies. After this, just run:
```
npm test
```