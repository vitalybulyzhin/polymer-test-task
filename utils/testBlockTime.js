const { ethers } = require("hardhat");

const blockTime = async (time) => {
    const blockNumAfter = await ethers.provider.getBlockNumber();
    const blockAfter = await ethers.provider.getBlock(blockNumAfter);
    const timestampAfter = blockAfter.timestamp;
    await network.provider.send("evm_increaseTime", [time - timestampAfter])
    await network.provider.send("evm_mine") 
} 

module.exports = blockTime