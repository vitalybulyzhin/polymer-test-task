const { ethers } = require("hardhat");

const testDeploy = async (price, startDate, endDate) => {
    try {
        const DutchAuction = await ethers.getContractFactory("DutchAuction");
        const contract = await DutchAuction.deploy(price, startDate, endDate);
        const deployed = await contract.deployed();
        return deployed
    } catch(e) {
        throw Error(e.message)
    }
}

module.exports = testDeploy