class TestPriceCalculator {
    a;
    b;
    price;
    startTime;
    endTime

    constructor(price, startTime, endTime) {
        this.a = price / (startTime - endTime)
        this.b = - this.a * endTime

        this.price = price;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    at(timestamp = this.startTime) {
        return this.a * timestamp + this.b
    }
}

module.exports = TestPriceCalculator