//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract DutchAuction is ERC20{
    address owner;
    // Coeficients for function
    int256 private a;
    int256 private b;

    // Values for initiate
    uint256 private startPrice;
    uint256 private startDate;
    uint256 private endDate;

    // Has completed
    bool completed = false;

    constructor(uint256 _startPrice, uint256 _startDate, uint256 _endDate) ERC20("VitaliiTestToken", "VTT") {
        validateInput(_startPrice, _startDate, _endDate);

        startPrice = _startPrice;
        startDate = _startDate;
        endDate = _endDate;

        a = int256(_startPrice) / (int256(_startDate) - int256(_endDate));
        b = - a * int256(_endDate);

        owner = msg.sender;
    }

    function getPrice() public view returns(
            int256 _price, 
            int256 _a, 
            int256 _b, 
            int256 _timeNow,
            string memory formula
        ) {
        int256 timeNow = int256(block.timestamp);
        int256 price = a * timeNow + b;

        if(price < 0) {
            price = 0;
        }
        
        return (
            price,
            a,
            b,
            timeNow,
            "function(a, b, timeStamp) { return a * timeStamp + b }"
        );
    }

    function bid() external payable returns(bool) {
        uint256 price = uint256(a * int256(block.timestamp) + b);

        require(block.timestamp > startDate, "Auction hasn't started yet");
        require(block.timestamp < endDate, "Sorry, auction has ended");
        require(msg.value >= price, "Insufficient balance has been send");
        require(!completed, "Sorry, no lots avalible");

        emit Received(msg.sender, msg.value);
        _mint(msg.sender, 1);
        completed = true;
        return true;
    }

    function getBalance() public view returns(uint256) {
        require(msg.sender == owner, "You cannot access this info");
        return address(this).balance;
    }

    // Internal to this contract functions
    event Received(address, uint);

    function validateInput(uint256 _startPrice, uint256 _startDate, uint256 _endDate) private view returns(bool) {
        uint256 nowTime = block.timestamp;

        require(_startPrice > 0, "Start price should be bigger than 0");
        require(_startDate > nowTime, "Start date/time is not valid");
        require(_endDate > nowTime, "End date/time is not valid");
        require(_endDate > _startDate, "End date/time should be bigger than start date/time");

        return true;
    }
}
